SHELL=/bin/bash
## General stuff
.PHONY: clean
clean:
	rm *~

## GIT stuff
##  Makefile rules to add, commit, pull and push
##  all default values are for this project, adapt as necessary
git_origin=ssh://git@bitbucket.org/steven_f_l_deprez/
repository=templates
sources=$(sources_base) $(sources_article)
sources_base=Makefile updateifchanged inited
sources_article=sdpDefinitions.tex sdpStyle.tex template.tex sdpabbrv.bst references.bib inited_article

%.added: %
	git add $<
	touch $<.added

commit: $(patsubst %,%.added,$(sources))
	git commit
	touch commit

push: commit
	git push -u origin master
	touch push

.PHONY: pull
pull: commit
	git pull origin

# Initialise a new folder with some template
#  The new folder is given on the command line as DEST, and the remote repository name is REPO
.PHONY: clone init init_article
clone:
	@if [[ ! -d $(DEST) ]]; then echo git clone $(git_origin)$(REPO) $(DEST); git clone $(git_origin)$(REPO) $(DEST); fi

init: clone
	@if [[ ! -f $(DEST)/inited ]]; then echo cp $(sources_base) $(DEST); cp $(sources_base) $(DEST); fi

init_article: init
	@if [[ ! -f $(DEST)/inited_article ]]; then echo cp $(sources_article) $(DEST); cp $(sources_article) $(DEST); fi

## TeX stuff
##  Makefile rules to compile LaTeX files, rerunning LaTeX as needed, running bibtex as needed and so on
##  Default values for variables are for the example provided

# we rerun LaTeX if one of the statefiles is updated
# but statefiles are rewritten every time LaTeX runs, so we really want to use diff instead of the timestamp
# the original file is saved as %.aux.ts, with the timestamp of the last change
%.pdf: %.tex %.aux.ts %.bbl Makefile
	if [[ -f $*.aux.done ]]; then rm $*.aux.done; fi
	pdflatex $<
	touch $*.aux.done
	$(MAKE) $@

%.bbl: %.aux.ts references.bib Makefile
	if [[ -f $*.aux.done ]]; then bibtex $*; else touch $@; fi

.PRECIOUS: %.ts
%.ts: % Makefile
	./updateifchanged $*

%.ts:
	touch $@
